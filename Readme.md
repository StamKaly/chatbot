An in-game chatbot for altitude.

At the moment, libalt doesn't implement the vapor protocol, so there's no
way to authenticate. To use this bot, you need to connect to a server with the
official client via a proxy and get the session ID, then disconnect from the
server but stay connected to vapor, and start the chatbot with that session ID.

The session ID will be valid until you close the official client.

## Commands


|Command | Description |
|--------|-------------|
|!ping  |  reply with "pong!"       |
