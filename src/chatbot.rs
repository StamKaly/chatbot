#[macro_use] extern crate mioco;
#[macro_use] extern crate log;
extern crate logger;
extern crate pupil;
extern crate libalt;

use libalt::dispatcher::*;
use libalt::event_mgr::*;
use libalt::message::*;
use libalt::packet::Context;
use libalt::net::game_msg::*;
use libalt::net::join_msg::*;
use libalt::net::version::VERSION;
use libalt::net::client_id::*;
use libalt::net::client_level::*;
use libalt::net::playerno::*;
use libalt::net::uuid::*;
use libalt::guarantee::Level as GLevel;
use std::net::SocketAddr;
use std::str::FromStr;

use libalt::event::*;

use mioco::Evented;
use mioco::sync::mpsc::*;

mod ping_hdr;

fn main(){
    logger::init();

    let mut mioco = mioco::Mioco::new_configured(libalt::mioco_conf());
    mioco.start(move ||{

        let mut disp = Dispatcher::new();
        disp.context_in = Context::Client;
        disp.context_out = Context::Client;

        let port = 27220;
        ping_hdr::start(&mut disp, port);

        let sx = disp.sender();
        let rx_game = disp.receiver::<GameMsg>(port);
        let rx_login = disp.receiver::<JoinMsg>(port);

        mioco::spawn(move || disp.run());

        //let (addr,pass) = ("127.0.0.1:27276","hai");
        let (addr,pass) = ("46.101.20.237:27276","ruleone"); //flightclub
        //let (addr,pass) = ("91.121.160.173:27276",""); //ledows

        let server = SocketAddr::from_str(addr).unwrap();

        let mut bot = ChatBot::new();
        let mut counter = 0;

        info!("Attempting to join server at {:?}", server);

        match join(&server, port, pass.to_owned(), bot.id.clone(), sx.clone(), &rx_login){
            Ok(c) => {
                counter = c;
                info!("Connected to server, counter = {}",counter);
            }
            Err(e) => {
                info!("Failed to join server: {:?}",e);
                return;
            }
        }


        let mut timer = mioco::timer::Timer::new();
        timer.set_timeout(20);

        let mut ev_mgr = EventMgr::new(sx, server, port);
        bot.counter = counter;
        ev_mgr.counter = counter;

        loop{
            unsafe{
                rx_game.select_add(mioco::RW::read());
                timer.select_add(mioco::RW::read());
                rx_login.select_add(mioco::RW::read());
            }
            let ret = mioco::select_wait();

            if ret.id() == rx_game.id(){
                if let Ok(msg) = try_recv::<GameMsg>(&rx_game){
                    ev_mgr.msg_in(msg.data);
                    while let Some(ev) = ev_mgr.next(){
                        bot.handle_event(ev, &mut ev_mgr);
                    }
                }
            }
            if ret.id() == timer.id(){
                ev_mgr.tick();
                timer.set_timeout(20);
            }

            if ret.id() == rx_login.id() {
                if let Ok(msg) = try_recv::<JoinMsg>(&rx_login){
                    match *msg.data{
                        JoinMsg::Disconnect => {
                            info!("Got disconnect from server, stopping.");
                            std::process::exit(0);
                        }
                        _ => ()
                    }
                }
            }
        }
    }).unwrap();
}

#[derive(Debug)]
pub enum JoinError{
    Timeout,
    JoinReqRefused(String),
}

use mioco::timer::Timer;

pub struct ChatBot{
    player_no: Option<PlayerNo>,
    pub id: ClientID,
    pub counter: u8,
}

impl ChatBot{
    pub fn new() -> ChatBot{
        ChatBot{
            player_no: Some(PlayerNo{value:1}),
            counter: 0,
            id: ClientID{
                nick:  "chatbot".to_owned(),
                addr:  None,
                vapor: Uuid::new(0x4802468ba896ce74,0x6b211c4cbff6d232),
                level: ClientLevel{
                    ace:    1,
                    level:  40,
                    kills:  1,
                    deaths: 1
                },
                login: true,
                uuid:  Some(Uuid::new(0xb5c34afd623d2407,0x254af984977d3415)),
                unk1:  40497503,
                unk2:  vec![0,0,0,0,0,0,0],
                unk3:  vec![0,0,0,0],
            }
        }

    }
    pub fn handle_event(&mut self, ev: Event, evm: &mut EventMgr){
        match ev{
            Event::PlayerInfo(ref x) => {
                info!("got player info ev");
                if x.join && x.uuid == self.id.vapor{
                    self.player_no = Some(x.id);
                    info!("Joined the game");
                    info!("My player ID is {:?}",self.player_no);
                }
            }
            Event::Chat(ref x) => {
                if let Some(id) = self.player_no{
                    if x.player != id {
                        self.handle_chat(x.clone(), evm);
                    }
                }
            }
            _ => ()
        }
    }

    fn handle_chat(&mut self, ev: ChatEv, evm: &mut EventMgr){
        let msg = &ev.msg;
        if msg == "!math 360noscope * 420" {
            self.say(" = mnt dew",evm);
        }
        if msg.starts_with("!"){
            let cmd = msg[1..].split(" ")
                .collect::<Vec<&str>>();
            if cmd.len() == 0 { return; }
            match cmd[0] {
                "ping" => self.say("pong!", evm),
                "playerno" => self.say(&format!("Your player number is: {:?}", ev.player), evm),
                "stam" => self.say("Stam's the best", evm),
                "fluffy" => self.say("fluffs is pro", evm),
                "math" => {
                    let s= cmd[1..].iter().fold("".to_owned(),|ac,x| ac + " " + x);
                    let env = pupil::Env::default();
                    let mut expr = pupil::Expr::new(&env);
                    if let Ok(_) = expr.feed(&s){
                        if let Ok(res) = expr.result(){
                            self.say(&format!("{} = {}", &s, res), evm);
                        }
                    }
                }
                _ => ()
            }
        }
    }

    pub fn say(&self, message: &str, evm: &mut EventMgr){
        if let Some(id) = self.player_no{
            let ev = Event::Chat(ChatEv{
                player: id,
                is_team: false,
                msg: message.to_owned()
            });
            evm.send(ev);
        }
    }
}

pub fn join(server: &SocketAddr, port: u16, pass: String, id: ClientID,
            sx: Sender<Msg>, rx: &Receiver<Msg>) -> Result<u8,JoinError>{

    let join_req_msg = Message::new(&server, port, GLevel::Order,
        JoinMsg::JoinReq( ReqData{
            version: VERSION,
            pass: pass,
            id: id}));

    send(&sx, join_req_msg);

    let mut timeout = Timer::new();
    timeout.set_timeout(20_000);

    loop {
        unsafe{
            timeout.select_add(mioco::RW::read());
            rx.select_add(mioco::RW::read());
        }
        let ret = mioco::select_wait();

        if ret.id() == timeout.id() {
            return Err(JoinError::Timeout);
        }
        if ret.id() == rx.id() {
            if let Ok(msg) = try_recv::<JoinMsg>(rx){
                match *msg.data{
                    JoinMsg::JoinResp(ref x) => {
                        info!("Got join response {:?}", x);
                        if let Some(ref reason) = x.reason{
                            return Err(JoinError::JoinReqRefused(reason.clone()));
                        }
                    }
                    JoinMsg::LoadResp(ref x) => {
                        info!("Got load response, current map: {:?}", x.resource);
                        let map = x.resource.clone();
                        let counter = x.counter;
                        send(&sx, Message::new(&server, port, GLevel::Order,
                            JoinMsg::LoadReq(LoadReqData{ resource: map })));
                        return Ok(counter);
                    }
                    _ => ()
                }
            }
        }
    }
}
